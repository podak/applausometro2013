import pygtk, gtk, string

class Classifica:
	
	def __init__(self,builder,pathClassifica):
		self.classificaTxtVw=builder.get_object ("viewclassifica")
		self.pathClassifica=pathClassifica
		self.concorrenti=[]		
		self.getClassifica(pathClassifica,self.classificaTxtVw.get_buffer())
		
	def getClassifica(self,path,txtBuff):
		stringfile=self.leggiFile(path)
		concorrente=["","",-360.000]
		concorrenti=[]
		txtbufstring=""
		if len(stringfile)>1:
			c=0
			
			while string.find(stringfile,"\n")!=(-1):
				None
				idx=string.find(stringfile,"\n")
				slice1=stringfile[0:idx]
				#print slice1
				idy=string.find(stringfile,";")
				nomeconc=slice1[0:idy]
				stringfile=string.replace(stringfile,nomeconc+";","")
				idx=string.find(stringfile,"\n")
				slice1=stringfile[0:idx]
				#print slice1
				idy=string.find(stringfile,";")
				cognomeconc=slice1[0:idy]
				stringfile=string.replace(stringfile,cognomeconc+";","")
				idx=string.find(stringfile,"\n")
				slice1=stringfile[0:idx]
				#print slice1
				idy=string.find(stringfile,"\n")
				scoreconc=float(slice1[0:idy])
				stringfile=string.replace(stringfile,str(scoreconc)+"\n","")
				concorrenti.append([scoreconc,nomeconc,cognomeconc])
				#print concorrenti[c]
				txtbufstring=txtbufstring+str(c+1)+"    "+concorrenti[c][1]+"    "+concorrenti[c][2]+"               Valore picco: "+str(concorrenti[c][0])+"\n"
				c=c+1			
		txtBuff.set_text(txtbufstring)
		self.concorrenti=concorrenti
		
	
	def leggiFile(self,path):
		classificaFile=open(path,'r')
		stringFile=classificaFile.read()
		classificaFile.close()
		return stringFile	
	
	def addConcorrenteLista(self,concorrente):
		self.concorrenti.append(concorrente)
		self.concorrenti.sort()
		self.concorrenti.reverse()
		txtBuffer=self.classificaTxtVw.get_buffer()
		txtbufstr=""
		for i in range(0,len(self.concorrenti)):
			txtbufstr=txtbufstr+str(i+1)+"    "+self.concorrenti[i][1]+"     "+self.concorrenti[i][2]+"     Picco[dB]: "+str(self.concorrenti[i][0])+"\n"
		txtBuffer.set_text(txtbufstr)
		classificafile=open(self.pathClassifica,'w')
		stringfile=""
		for i in range(0,len(self.concorrenti)):
			stringfile=stringfile+self.concorrenti[i][1]+";"+self.concorrenti[i][2]+";"+str(self.concorrenti[i][0])+"\n"
		classificafile.write(stringfile)
		classificafile.close()	
		
		
