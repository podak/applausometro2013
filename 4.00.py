import pygtk, gtk, gobject
from UIMisurazioneRumore import *
from UISecondaColonna import *
from UIClassifica import *
from Microfono import *

class interfacciaGrafica:
	
	def __init__(self):
		self.pathClassifica="/home/davide/Scrivania/applausometro/software/4.00 (2013)/Classifica.appl"
		print("interfaccia avviata con successo")
		builder=gtk.Builder()
		builder.add_from_file('/home/davide/Scrivania/applausometro/software/4.00 (2013)/window.glade')
		builder.connect_signals(self)
		self.misRum=UIMisurazioneRumore(builder)
		self.applausometro=UIApplausometro(builder)
		self.datiConcorrente=DatiConcorrente(builder)
		self.taratura=Taratura(builder)
		self.giochiLuci=GiochiLuci(builder)
		self.classifica=Classifica(builder,self.pathClassifica)
		self.window=builder.get_object ("window1")
		self.window.show_all()
	
	def recini(self,button):
		print ("rec button pressed")
		self.misRum.piccomax.set_sensitive(True)
		self.misRum.appbut.set_sensitive(True)
		self.misRum.fisbutt.set_sensitive(True)
		self.datiConcorrente.buttRec.set_sensitive(False)
		self.datiConcorrente.buttEndRec.set_sensitive(True)
		self.datiConcorrente.editNome.set_sensitive(False)
		self.datiConcorrente.editCognome.set_sensitive(False)
		self.taratura.buttTaratura.set_sensitive(False)
		self.giochiLuci.buttSupercar.set_sensitive(False)
		self.giochiLuci.buttAlternato1.set_sensitive(False)
		self.giochiLuci.buttAlternato2.set_sensitive(False)
		self.giochiLuci.buttStop.set_sensitive(False)
		self.applausometro.accendi("rec")
	
	def recfin(self,button):
		self.misRum.piccomax.set_sensitive(False)
		self.misRum.appbut.set_sensitive(False)
		self.misRum.fisbutt.set_sensitive(False)
		self.datiConcorrente.buttRec.set_sensitive(True)
		self.datiConcorrente.buttEndRec.set_sensitive(False)
		self.datiConcorrente.editNome.set_sensitive(True)
		self.datiConcorrente.editCognome.set_sensitive(True)
		self.taratura.buttTaratura.set_sensitive(True)
		self.giochiLuci.buttSupercar.set_sensitive(True)
		self.giochiLuci.buttAlternato1.set_sensitive(True)
		self.giochiLuci.buttAlternato2.set_sensitive(True)
		self.giochiLuci.buttStop.set_sensitive(True)
		media=self.mediaPicchi(self.applausometro.picchi)
		self.applausometro.spegni()
		self.applausometro.piccoMax=-300.00
		if self.misRum.fisbutt.get_active() :
			media=-300.00-media
			self.misRum.fisbutt.set_active(False)
			self.misRum.appbut.set_active(True)
		self.classifica.addConcorrenteLista([media,self.datiConcorrente.editNome.get_text(),self.datiConcorrente.editCognome.get_text()])
		self.datiConcorrente.editNome.set_text("")
		self.datiConcorrente.editCognome.set_text("")
		
	def startTaratura(self,button):
		self.applausometro.picchiMin=[0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00]
		self.applausometro.picchi=[-300.00,-300.00,-300.00,-300.00,-300.00,-300.00,-300.00,-300.00,-300.00,-300.00]
		self.applausometro.accendi("taratura")
	
	def mediaPicchi(self,arrayValori):
		media=0
		for i in range (0,len(arrayValori)):
			media=media+arrayValori[i]
		return (media/10)
		
ui=interfacciaGrafica()
print(ui)
gtk.main()
		
