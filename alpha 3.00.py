#!/usr/bin/env python
import sys, os
import pygtk, gtk, gobject
import pygst
pygst.require("0.10")
import gst,string
import usb.core
import usb.util,time

class Handler:
		def recini(self,button):
			global recflag,appbut,fisbutt,recbutt,endrecbutt,nomedit,cognomedit,piccomax,piccomassimo,livappl
			print "REGISTRAZIONE INIZIATA..."
			recflag=True
			piccomax.set_sensitive(True)
			appbut.set_sensitive(True)
			fisbutt.set_sensitive(True)
			recbutt.set_sensitive(False)
			endrecbutt.set_sensitive(True)
			nomedit.set_sensitive(False)
			cognomedit.set_sensitive(False)
			piccomassimo=-300.00
			livappl=0
			
		def recfin(self,button):
			global recflag,appbut,fisbutt,recbutt,endrecbutt,nomedit,cognomedit,piccomax,picchi,concorrenti,txtbuff,livappl
			print "REGISTRAZIONE FINITA..."
			recflag=False
			piccomax.set_sensitive(False)
			appbut.set_sensitive(False)
			fisbutt.set_sensitive(False)
			recbutt.set_sensitive(True)
			endrecbutt.set_sensitive(False)
			nomedit.set_sensitive(True)
			cognomedit.set_sensitive(True)
			piccomedio=0
			for i in range (0,10):
				piccomedio=piccomedio+picchi[i]
			piccomedio=piccomedio/10
			if fisbutt.get_active()==True :
				piccomedio=-300.00-piccomedio
				fisbutt.set_active(False)
				appbut.set_active(True)
			picchi=[-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000]
			#print piccomedio
			concorrenti.append([piccomedio,nomedit.get_text(),cognomedit.get_text()])
			concorrenti.sort()
			concorrenti.reverse()
			#print concorrenti
			txtbufstr=""
			for i in range(0,len(concorrenti)):
				txtbufstr=txtbufstr+str(i+1)+"    "+concorrenti[i][1]+"     "+concorrenti[i][2]+"     Picco[dB]: "+str(concorrenti[i][0])+"\n"
			txtbuff.set_text(txtbufstr)
			classificafile=open("/home/davide/Scrivania/applausometro/software/3.00/Classifica.appl",'w')
			stringfile=""
			for i in range(0,len(concorrenti)):
				stringfile=stringfile+concorrenti[i][1]+";"+concorrenti[i][2]+";"+str(concorrenti[i][0])+"\n"
			classificafile.write(stringfile)
			classificafile.close()	
			cognomedit.set_text("")
			nomedit.set_text("")
			livappl=0
			
		
			
			
			
						
			
	
	

class GTK_Main:
	
		
	
	
		
   #############procedura misurazione picco#################     
        def agg_appl(self):
			global dev,livappl,conn
			if conn==1:
				for i in range(0,3):
					try:
						dev.write(1,chr(livappl+97),0,100)
					except:
						conn=0
						print("invio fallito")
					#time.sleep(5)
				#print "inviato"
			print chr(livappl+97)
			return True
			
	def radarcomandi(self):
		global conn,dev
		if conn==0 :
			try:
				dev = usb.core.find(idVendor=0x1993, idProduct=0x3333)	
				try:
					dev.detach_kernel_driver(0)
				except:
					print ('Gia detachato')
				dev.set_configuration()
				conn=1
				#self.dev.write(EP_OUT0,'=',0,100)
				#time.sleep(100)
				#self.dev.write(EP_OUT0,'=',0,100)
				#while cont_delay<5 :
				#	self.dev.write(EP_OUT0,'=',0,100)
				#	time.sleep(50)
				#	cont_delay=cont_delay+1
			except:
				print ('Nessun dispositivo rilevato')
		return True
		
	def controllo_connessione(self):
		global dev,conn
		print 'interruzione2'
		try:
			dev.write(1,'?',0,100)
		except:
			conn=0
			#self.labconn.set_text('Device non connesso')
		return True
			
        def stampa_picchi(bus,messaggio,level):
			global vumeter,piccomassimo,picchi,dev,livappl
			if messaggio.src is not level:
				return
			if not messaggio.structure.has_key("peak"):
				return
			if (piccomassimo<messaggio.structure["peak"][0]):
				piccomassimo=messaggio.structure["peak"][0]
				piccomax.set_text(str(piccomassimo)+" dBm")
			#print "Picco suono: ", messaggio.structure["peak"][0], " dBm"
			piccolive.set_text((str(messaggio.structure["peak"][0])+" dBm"))
			livello=int(-(messaggio.structure["peak"][0]) / 5)
			if recflag==True:
				if messaggio.structure["peak"][0]>picchi[9]:
					picchi[9]=messaggio.structure["peak"][0]
					picchi.sort()
					picchi.reverse()#reverse 
					#print picchi
				livappl=10-livello/2
				print livappl
				#EP_IN0=129 #indirizzo endpoint di ingresso rispetto al computer
				#EP_OUT0=1  #indirizzo endpoint di uscita rispetto al computer
				#try:
				#dev=usb.core.find(idVendor=0x1993, idProduct=0x3333)
				#	try:
				#		dev.detach_kernel_driver(0)
				#	except:
				#		print "gia detachato"
				#	dev.set_configuration()
				#	dev.write(EP_OUT0,chr(livappl+97),1,100)
				#dev.set_configuration()
				#dev.write(EP_OUT0,chr(livappl+97),0,10)
				#time.sleep(100)
				#except:
				#	print ('Nessun dispositivo rilevato')
				
			for i in range (1,len(vumeter)):
				if i<=livello:
					if i<5 :
						vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_rossospe.png")
					elif i<13:
						vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_giallospe.png")
					elif i>12:
						vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_verdespe.png")
				elif i>livello:
					if i<5 :
						vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_rosso.png")
					elif i<13:
						vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_giallo.png")
					elif i>12:
						vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_verde.png")
						
				
					
			
	##########################################################################
		
	
	

	def __init__(self):
		global piccomassimo
		global piccomax,piccolive
		global recflag,appbut,fisbutt,recbutt,endrecbutt,nomedit,cognomedit,dev,livappl,conn
		conn=0
		recflag=False
		livappl=0
		#dev=usb.core.find(idVendor=0x1993, idProduct=0x3333)
		#dev.set_configuration()
		try:
			dev.detach_kernel_driver(0)
		except:
			print "gia detachato"
		piccomassimo=-300.0000000
		builder=gtk.Builder()
		builder.add_from_file('/home/davide/Scrivania/applausometro/software/3.00/window.glade')
		builder.connect_signals(Handler())
		window=self.window=builder.get_object ("window1")
		scala=builder.get_object("scala")
		scala.set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/scala2.png")
		boxbricks=builder.get_object ("vboxvumeter")
		piccolive=builder.get_object ("vlivepic")
		piccomax=builder.get_object ("vmaxpic")
		appbut=builder.get_object ("applausibut")
		fisbutt=builder.get_object ("fischibut")
		recbutt=builder.get_object ("rec")
		endrecbutt=builder.get_object ("recstop")
		nomedit=builder.get_object ("nome")
		cognomedit=builder.get_object ("cognome")
		####creazione elementi scala livello suono####
		brick1=gtk.Image()
		brick2=gtk.Image()
		brick3=gtk.Image()
		brick4=gtk.Image()
		brick5=gtk.Image()
		brick6=gtk.Image()
		brick7=gtk.Image()
		brick8=gtk.Image()
		brick9=gtk.Image()
		brick10=gtk.Image()
		brick11=gtk.Image()
		brick12=gtk.Image()
		brick13=gtk.Image()
		brick14=gtk.Image()
		brick15=gtk.Image()
		brick16=gtk.Image()
		brick17=gtk.Image()
		brick18=gtk.Image()
		brick19=gtk.Image()
		brick20=gtk.Image()
		brick21=gtk.Image()
		global vumeter
		vumeter=[brick1,brick2,brick3,brick4,brick5,brick6,brick7,brick8,brick9,brick10,brick11,
		brick12,brick13,brick14,brick15,brick16,brick17,brick18,brick19,brick20,brick21]
		###inserimento elementi grafici nella window####
		for i in range(1,len(vumeter)):
			if i<5 :
				vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_rossospe.png")
			elif i<13:
				vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_giallospe.png")
			elif i>12:
				vumeter[i].set_from_file("/home/davide/Scrivania/applausometro/software/3.00/image_reference/brick_verdespe.png")
			boxbricks.pack_start(vumeter[i],False)
		############## misuratore suono completo #########
		############## recupero dati da file e scrittura classifica ############
		classificafile=open("/home/davide/Scrivania/applausometro/software/3.00/Classifica.appl",'r')
		stringfile=classificafile.read()
		classificafile.close()
		#print ("stingfile :"+stringfile)
		#print len(stringfile)
		global viewclassifica,concorrenti,picchi,txtbuff
		picchi=[-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000]
		viewclassifica=builder.get_object ("viewclassifica")
		txtbuff=viewclassifica.get_buffer()
		concorrente=["","",-360.000]
		concorrenti=[]
		txtbufstring=""
		if len(stringfile)>1:
			c=0
			
			while string.find(stringfile,"\n")!=(-1):
				None
				idx=string.find(stringfile,"\n")
				slice1=stringfile[0:idx]
				#print slice1
				idy=string.find(stringfile,";")
				nomeconc=slice1[0:idy]
				stringfile=string.replace(stringfile,nomeconc+";","")
				idx=string.find(stringfile,"\n")
				slice1=stringfile[0:idx]
				#print slice1
				idy=string.find(stringfile,";")
				cognomeconc=slice1[0:idy]
				stringfile=string.replace(stringfile,cognomeconc+";","")
				idx=string.find(stringfile,"\n")
				slice1=stringfile[0:idx]
				#print slice1
				idy=string.find(stringfile,"\n")
				scoreconc=float(slice1[0:idy])
				stringfile=string.replace(stringfile,str(scoreconc)+"\n","")
				concorrenti.append([scoreconc,nomeconc,cognomeconc])
				#print concorrenti[c]
				txtbufstring=txtbufstring+str(c+1)+"    "+concorrenti[c][1]+"    "+concorrenti[c][2]+"               Valore picco: "+str(concorrenti[c][0])+"\n"
				c=c+1			
		txtbuff.set_text(txtbufstring)
		########################################################################
		gobject.timeout_add(150, self.agg_appl)
		gobject.timeout_add(200, self.controllo_connessione)
		gobject.timeout_add(200, self.radarcomandi)
		window.show_all()###'''
        pipeline = gst.parse_launch ('pulsesrc ! audioconvert ! '
    'audio/x-raw-int,channels=1,rate=44100,endianness=1234,'
    'width=16,depth=16,signed=(bool)True !'
    'level name=level interval=80000000 !'
    'autoaudiosink')
        level = pipeline.get_by_name('level')
        bus = pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message', stampa_picchi,level)
        pipeline.set_state(gst.STATE_PLAYING)
      
        
        
  
     
	
		
	

	
GTK_Main()
gtk.main()
