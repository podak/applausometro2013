import pygtk, gtk

class DatiConcorrente:
	
	def __init__(self, builder):
		self.buttRec=builder.get_object ("rec")
		self.buttEndRec=builder.get_object ("recstop")
		self.editNome=builder.get_object ("nome")
		self.editCognome=builder.get_object ("cognome")

class Taratura:
	
	def __init__(self, builder):
		self.buttTaratura=builder.get_object ("taraturaBut")
		self.consoleTaratura=builder.get_object ("taraturaConsole")
		
class GiochiLuci:
	
	def __init__(self,builder):
		self.buttSupercar=builder.get_object ("supercar")
		self.buttAlternato1=builder.get_object ("alternato1")
		self.buttAlternato2=builder.get_object ("alternato2")
		self.buttStop=builder.get_object ("stop")
		
