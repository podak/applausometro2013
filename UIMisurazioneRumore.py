import pygtk, gtk, gobject
from Microfono import *
from UsbControl import *

pathPrefix="/home/davide/Scrivania/applausometro/software/4.00 (2013)"

class UIMisurazioneRumore:
	
	def __init__(self, builder):
		self.bulider=builder
		self.scala=builder.get_object("scala")
		scalaPath=pathPrefix+"/image_reference/scala2.png"
		self.scala.set_from_file(scalaPath)
		self.boxbricks=builder.get_object ("vboxvumeter")
		self.piccolive=builder.get_object ("vlivepic")
		self.piccomax=builder.get_object ("vmaxpic")
		self.appbut=builder.get_object ("applausibut")
		self.fisbutt=builder.get_object ("fischibut")		
		
		
class UIApplausometro:
	
	def __init__(self,builder):
		immEmpty=gtk.Image()
		self.audioMeter=[immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty,immEmpty]
		self.boxbricks=builder.get_object ("vboxvumeter")
		for i in range(0,20):
			imm=""
			self.audioMeter[i]=gtk.Image()
			if i<5 :
				imm=pathPrefix+"/image_reference/brick_rossospe.png"
			elif i<13:
				imm=pathPrefix+"/image_reference/brick_giallospe.png"
			elif i>12:
				imm=pathPrefix+"/image_reference/brick_verdespe.png"
			self.audioMeter[i].set_from_file(imm)
			self.boxbricks.pack_start(self.audioMeter[i],False)
		self.microfono=Microfono()
		self.interruttore=False
		self.timeOutProc=None
		self.consoleTaratura=builder.get_object ("taraturaConsole")
		self.offset=0.00;
		self.fondoScala=0.00;
		self.picchi=[-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000]
		self.picchiMin=[0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00]
		self.piccoMax=-300.00
		self.piccoMin=0.000
		self.taraOn=False
		self.recOn=False
		self.usb=Usb()
		self.arrayLampade=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		self.editPiccoLive=builder.get_object ("vlivepic")
		self.editPiccoMax=builder.get_object ("vmaxpic")
		gobject.timeout_add(50, self.aggiornaLivelloApplausometro)
		
	def accendi(self,mode):
		self.spegni()
		if (mode=="rec"):
			self.recOn=True
			return
		elif (mode=="supercar"):
			self.timeOutProc=gobject.timeout_add(150, self.supercarEffect)
		elif (mode=="alternato1"):
			self.timeOutProc=gobject.timeout_add(150, self.alternato1Effect)
		elif (mode=="alternato2"):
			self.timeOutProc=gobject.timeout_add(150, self.alternato2Effect)
		elif (mode=="taratura"):
			buffTxt=self.consoleTaratura.get_buffer()
			self.usb.inviaLivello(5)
			self.timeOutProc=gobject.timeout_add(5000, self.taratura)
			self.taraOn=True
			buffTxt.set_text("...inizio procedura di taratura...\n Per 5 secondi tenere il livello minimo di rumore")
			self.arrayLampade=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
		else:
			return
		self.interruttore=True

	
	def spegni(self):
		if (self.recOn):
			self.recOn=False
			self.usb.inviaLivello(0)
		elif (self.taraOn):
			self.taraOn=False
			gobject.source_remove(self.timeOutProc)
			self.interruttore=False
		elif (self.interruttore) and (not self.recOn):
			gobject.source_remove(self.timeOutProc)
			self.interruttore=False
		self.picchi=[-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000,-300.000]
		self.piccoMax=-300.00
		self.piccoMin=0.000
	
	def aggiornaLivelloApplausometro(self):
		piccoRec=self.microfono.getLivelloVolume()
		if (self.piccoMax<piccoRec):
			self.piccoMax=piccoRec
			self.editPiccoMax.set_text((str(piccoRec)+" dBm"))
		if (self.piccoMin>piccoRec):
			self.piccoMin=piccoRec
		self.editPiccoLive.set_text((str(piccoRec)+" dBm"))
		livello=20-int((piccoRec+300-self.offset)*20/(self.fondoScala+300-self.offset))
		if (self.recOn) and (piccoRec>self.picchi[9]):
			self.picchi[9]=piccoRec
			self.picchi.sort()
			self.picchi.reverse()
		elif (self.taraOn):
			if (piccoRec>self.picchi[9]):
				self.picchi[9]=piccoRec
				self.picchi.sort()
				self.picchi.reverse()
			elif (piccoRec<self.picchiMin[9]):
				self.picchiMin[9]=piccoRec
				self.picchiMin.sort()
		if not( self.interruttore):
			self.setLampadeApplausometro(livello)
		if not( self.interruttore) and (self.recOn):
			livelloUsb=20-livello
			self.usb.inviaLivello(int(livelloUsb/2))
			print (livelloUsb, int(livelloUsb/2))
		#print (livello,"\t",20-livello)
		#print (piccoRec)
		return True
	
	def taratura(self):
		txtBuff=self.consoleTaratura.get_buffer()
		arr1=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
		arr2=[1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0]
		print("interrupt")
		if (self.arrayUguale(self.arrayLampade,arr1)):
			self.arrayLampade=arr2
			self.setLampadeApplausometroArr()
			self.offset=self.mediaArray(self.picchiMin)+300
			txtBuff.set_text("\n offset regolato\n Per 5 secondi tenere il massimo livello di rumore")
			self.usb.inviaLivello(10)
			print("Registrazione offset: ")
			print(self.offset)
			self.arrayLampade=[1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1,1,0,0]
		elif (self.arrayUguale(self.arrayLampade,arr2)):
			self.arrayLampade=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			self.setLampadeApplausometroArr()
			self.fondoScala=self.mediaArray(self.picchi)
			txtBuff.set_text("\n fondo scala regolato\n procedura terminata con successo.")
			self.spegni()
			self.usb.inviaLivello(0)
			print("Registrazione fondo scala : ")
			print(self.fondoScala)
			return False
		return True
		
	def mediaArray(self,arrayValori):
		media=0
		for i in range (0,len(arrayValori)):
			media=media+arrayValori[i]
		return (media/len(arrayValori))
	
	def arrayUguale(self,arr1,arr2):
		print (arr1,arr2)
		if not (len(arr1)==len(arr2)):
			return False
		for i in range(0,len(arr1)-1):
			if not (arr1[i]==arr2[i]):
				return False
		return True	

	def setLampadeApplausometro(self,livello):
		for i in range (1,len(self.audioMeter)-1):
			if i<=livello:
				if i<5 :
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_rossospe.png")
				elif i<13:
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_giallospe.png")
				elif i>12:
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_verdespe.png")
				self.arrayLampade[i]=0
			elif i>livello:
				if i<5 :
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_rosso.png")
				elif i<13:
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_giallo.png")
				elif i>12:
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_verde.png")
				self.arrayLampade[i]=1
	
	def setLampadeApplausometroArr(self):
		array=self.arrayLampade
		for i in range (0,len(self.audioMeter)-1):
			if array[i]==0:
				if i<5 :
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_rossospe.png")
				elif i<13:
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_giallospe.png")
				elif i>12:
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_verdespe.png")
				self.arrayLampade[i]=0
			elif array[i]==1:
				if i<5 :
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_rosso.png")
				elif i<13:
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_giallo.png")
				elif i>12:
					self.audioMeter[i].set_from_file(pathPrefix+"/image_reference/brick_verde.png")
				self.arrayLampade[i]=1

		

		
		
		
		
