import pygst
pygst.require("0.10")
import gst

class Microfono:
	
	def __init__(self):
		self.livelloVolume=-300.000
		self.pipeline = gst.parse_launch('pulsesrc ! audioconvert ! audio/x-raw-int,channels=1,rate=44100,endianness=1234, width=16,depth=16,signed=(bool)True ! level name=level interval=80000000 ! autoaudiosink')
		self.level = self.pipeline.get_by_name('level')
		self.bus = self.pipeline.get_bus()
		self.bus.add_signal_watch()
		self.bus.connect('message', self.aggiornaLivelloVolume,self.level)
		self.pipeline.set_state(gst.STATE_PLAYING)
        
	def aggiornaLivelloVolume(self,bus,messaggio,level):
		if messaggio.src is not level:
			return
		if not messaggio.structure.has_key("peak"):
			return
		self.livelloVolume=messaggio.structure["peak"][0]
		
	def getLivelloVolume(self):
		return self.livelloVolume
